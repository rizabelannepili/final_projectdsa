import pwinput
import csv
import time
from game.game_menu import game_menu


def login():
    myfile = 'database.csv'
    print('Login Form')
    count = 0
    while count <= 2:
        username = input('Username: ')
        while username == '':
            print('empty username')
            username = input('Username: ')
        password = pwinput.pwinput(prompt='Password: ', mask='*')
        with open(myfile) as csv_file:
            csv_read = csv.reader(csv_file, delimiter=',')
            is_success = False
            for row in csv_read:
                if len(row) > 1:
                    if row[0] == username and row[1] == password:
                        is_success = True
                        break
            else:
                print('Login Failed')
                count += 1
        if is_success:
            print('Login Success')
            # time.sleep(3)
            game_menu()
            break            
    else:
        print('3 attempts Logo...Exit')
        # time.sleep(3)
    global is_exit
    global main_menu_input
    is_exit = True
    main_menu_input = ''
    # time.sleep(10)